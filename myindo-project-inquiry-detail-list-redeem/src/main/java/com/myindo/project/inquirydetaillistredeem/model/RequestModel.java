/**
 * 
 */
package com.myindo.project.inquirydetaillistredeem.model;

/**
 * @author Resti Pebriani
 *
 */
public class RequestModel {
	private String npk;
	private String idList;

	public String getNpk() {
		return npk;
	}

	public void setNpk(String npk) {
		this.npk = npk;
	}

	public String getIdList() {
		return idList;
	}

	public void setIdList(String idList) {
		this.idList = idList;
	}
}
