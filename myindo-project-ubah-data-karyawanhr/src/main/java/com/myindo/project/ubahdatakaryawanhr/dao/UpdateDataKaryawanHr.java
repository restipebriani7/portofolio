/**
 * 
 */
package com.myindo.project.ubahdatakaryawanhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.myindo.project.ubahdatakaryawanhr.connection.Connect;
import com.myindo.project.ubahdatakaryawanhr.model.HasilGet;
import com.myindo.project.ubahdatakaryawanhr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class UpdateDataKaryawanHr {

	Logger log = Logger.getLogger("UpdateDataKaryawanHr");
	HasilGet listKary;
	String hasilGet;
	String path;

	public String updateDb(RequestModel reModel) throws Exception {
		String codeResponse = "";
		Connection con = Connect.connection();

		try {
			PreparedStatement po = con.prepareStatement("SELECT npk FROM karyawan WHERE npk=?");
			po.setString(1, reModel.getNpk());
			ResultSet ro = po.executeQuery();
			while (ro.next()) {
				listKary = new HasilGet();
				listKary.setNpk(ro.getString(1));
			}
			ro.close();
			String npk = listKary.getNpk();
//			String split = npk.substring(6);
			System.out.println("Npk : "+npk);
			
			
//			//penamaan foto
//			Random random =  new Random();
//			int foto = random.nextInt(100000);
//			String fotoString = Integer.toString(foto);
//			
//			// decode base64 to image and save to directory
//			String base64String = reModel.getFoto();
//
//			// convert base64 string to binary data
//			byte[] data = DatatypeConverter.parseBase64Binary(base64String);
//			path = "C:\\Users\\Resti Pebriani\\workspace\\picture\\UPKYW" + fotoString
//					+ ".jpg";
//
//			File file = new File(path);
//			System.out.println("Picture disimpan di " + path);
//
//			try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
//				outputStream.write(data);
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			
			if (!reModel.getNpk().trim().isEmpty() && reModel.getNpk() != null) {
				PreparedStatement ps = con.prepareStatement(
						"UPDATE karyawan SET nama=?, status_karyawan=?, tgl_bergabung=?, tgl_resign=?, id_jabatan=?, id_department=?,"
						+ " tmpt_lahir=?, tgl_lahir=?, alamat_ktp=?, alamat_skrg=?, agama=?, status_pernikahan=?, jns_kelamin=?, "
						+ "gol_darah=?, no_telp=?, email=?, npwp=?, id_asuransi=?, id_tabungan=?, id_pendidikan=?, role=? "
						+ "WHERE npk=?");

				ps.setString(22, reModel.getNpk());
				ps.setString(1, reModel.getNama());
				ps.setString(2, reModel.getStatus_karyawan());
				ps.setString(3, reModel.getTgl_bergabung());
				ps.setString(4, reModel.getTgl_resign());
				ps.setString(5, reModel.getId_jabatan());
				ps.setString(6, reModel.getId_department());
				ps.setString(7, reModel.getTmpt_lahir());
				ps.setString(8, reModel.getTgl_lahir());
				ps.setString(9, reModel.getAlamat_ktp());
				ps.setString(10, reModel.getAlamat_skrg());
				ps.setString(11, reModel.getAgama());
				ps.setString(12, reModel.getStatus_pernikahan());
				ps.setString(13, reModel.getJns_kelamin());
				ps.setString(14, reModel.getGol_darah());
				ps.setString(15, reModel.getNo_telp());
				ps.setString(16, reModel.getEmail());
				ps.setString(17, reModel.getNpwp());
				ps.setString(18, reModel.getId_asuransi());
				ps.setString(19, reModel.getId_tabungan());
				ps.setString(20, reModel.getId_pendidikan());
				ps.setString(21, reModel.getRole());

				ps.executeUpdate();

				codeResponse = "0000";
				log.info("Update Data is Success");
			}else {
				codeResponse = "1111";
				log.info("NPK tidak ditemukan");
			}
			con.close();
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Update Data is Failed");
			log.info(e.getMessage());
		}
		return codeResponse;

	}

}
