/**
 * 
 */
package com.myindo.project.login.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.login.connection.Connect;
import com.myindo.project.login.model.HasilGet;
import com.myindo.project.login.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class GetLogin {
	Logger log = Logger.getLogger("GetLogin");
	HasilGet hasilGet;
	String result;	

	public String login(RequestModel rModel) throws Exception {
		Connection con = Connect.connection();
		hasilGet = new HasilGet();

		try {
			PreparedStatement ps = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			ps.setString(1, rModel.getNpk());
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				hasilGet.setNpk(rs.getString("npk"));
				hasilGet.setNama(rs.getString("nama"));
				hasilGet.setPasswd(rs.getString("password"));
				hasilGet.setRole(rs.getString("role"));
			}
			rs.close();
			System.out.println("Npk : " + rModel.getNpk());
			if (!hasilGet.getNpk().trim().isEmpty() && hasilGet.getNpk() != null) {
				ObjectMapper mapper = new ObjectMapper();
				result = mapper.writeValueAsString(hasilGet);
				System.out.println("Hasil get karyawan : " + result);

				log.info("Login berhasil");
				con.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
			log.info("Login Gagal");
			log.info(e.getMessage());
			hasilGet = new HasilGet();

			hasilGet.setNpk("");
			hasilGet.setNama("");
			hasilGet.setPasswd("");

			ObjectMapper mapper = new ObjectMapper();
			result = mapper.writeValueAsString(hasilGet);
			System.out.println("Hasil catch product : " + hasilGet);
		}
		return result;
	}
}
