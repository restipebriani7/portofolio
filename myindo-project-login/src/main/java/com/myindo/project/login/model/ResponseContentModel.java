/**
 * 
 */
package com.myindo.project.login.model;

/**
 * @author Resti Pebriani
 *
 */
public class ResponseContentModel {
	private String npk;
	private String nama;
	private String passwd;
	private String role;
	public String getNpk() {
		return npk;
	}
	public void setNpk(String npk) {
		this.npk = npk;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
}
