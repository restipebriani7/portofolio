/**
 * 
 */
package com.myindo.project.login.proses;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.login.dao.GetLogin;
import com.myindo.project.login.model.HasilGet;
import com.myindo.project.login.model.RequestModel;
import com.myindo.project.login.model.ResponseContentModel;
import com.myindo.project.login.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesLogin implements Processor{
	Logger log = Logger.getLogger("ProsesLogin");
	ObjectMapper mapper = new ObjectMapper();

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();

		RequestModel requestModel = gson.fromJson(body, RequestModel.class);
		GetLogin gLogin = new GetLogin();

		String codeResponse = gLogin.login(requestModel);
		
		HasilGet hasilGet = gson.fromJson(codeResponse, HasilGet.class);

		String ser = response(hasilGet);
		Response response = Response.status(HttpStatus.OK_200).entity(ser)
				.header(Exchange.CONTENT_TYPE, "application/json").build();
		exchange.getOut().setBody(response);
	}

	private String response(HasilGet hasilGet) {
		// TODO Auto-generated method stub
		String response = "";
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel contentModel = new ResponseContentModel();
			
			if (hasilGet.getNpk().trim().isEmpty()) {
				responseModel.setResponseCode("1111");
				responseModel.setResponseMessage("Failed");
				contentModel.setNpk("");
				contentModel.setNama("");
				contentModel.setPasswd("");
				contentModel.setRole("");
			} else {
				responseModel.setResponseCode("0000");
				responseModel.setResponseMessage("Success");
				contentModel.setNpk(hasilGet.getNpk());
				contentModel.setNama(hasilGet.getNama());
				contentModel.setPasswd(hasilGet.getPasswd());
				contentModel.setRole(hasilGet.getRole());
			}
			
			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int mount = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);

			responseModel.setDate(("" + year + "-" + mount + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(contentModel);

			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getMessage());
		}
		return response;
	}
}
