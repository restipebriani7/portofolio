/**
 * 
 */
package com.myindo.project.adddatakaryawanhr.processor;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Logger;

import javax.ws.rs.core.Response;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.eclipse.jetty.http.HttpStatus;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myindo.project.adddatakaryawanhr.dao.InsertDataKaryawanHr;
import com.myindo.project.adddatakaryawanhr.model.RequestModel;
import com.myindo.project.adddatakaryawanhr.model.ResponseContentModel;
import com.myindo.project.adddatakaryawanhr.model.ResponseModel;

/**
 * @author Resti Pebriani
 *
 */
public class ProsesAddKaryawanHr implements Processor{
	ObjectMapper mapper = new ObjectMapper();
	Logger log = Logger.getLogger("ProsesAddKaryawanHr");

	@Override
	public void process(Exchange exchange) throws Exception {
		// TODO Auto-generated method stub
		
		String body = exchange.getIn().getBody(String.class);
		Gson gson = new Gson();
		RequestModel rModel = gson.fromJson(body, RequestModel.class);
		
		InsertDataKaryawanHr iKaryawanHr = new InsertDataKaryawanHr();
		
		String codeResponse = iKaryawanHr.addDb(rModel);
		System.out.println("CodeResponse : " +codeResponse);
		
		String string = response(codeResponse);
		
		Response response = Response.status(HttpStatus.OK_200)
				.entity(string)
				.header(Exchange.CONTENT_TYPE, "application/json")
				.build();
		exchange.getOut().setBody(response);
	}
	
	private String response(String codeResponse) {
		// TODO Auto-generated method stub
		String response = "";
		
		try {
			ResponseModel responseModel = new ResponseModel();
			ResponseContentModel rContentModel = new ResponseContentModel();
			
			if(codeResponse.equals("0000")) {
				responseModel.setResponseCode(codeResponse);
				responseModel.setResponseMessage("Success");
				rContentModel.setMessage("Data Berhasil di Tambahkan");
			}else if(codeResponse.equals("1111")) {
				responseModel.setResponseCode(codeResponse);
				responseModel.setResponseMessage("Failed");
				rContentModel.setMessage("Data Gagal di Tambahkan");
			}
			
			GregorianCalendar gr = new GregorianCalendar();

			int year = gr.get(Calendar.YEAR);
			int month = gr.get(Calendar.MONTH) + 1;
			int day = gr.get(Calendar.DAY_OF_MONTH);
			int hour = gr.get(Calendar.HOUR_OF_DAY);
			int minute = gr.get(Calendar.MINUTE);
			int second = gr.get(Calendar.SECOND);
			
			rContentModel.setCode(codeResponse);
			responseModel.setDate(("" + year + "-" + month + "-" + "" + day).toString());
			responseModel.setTime(("" + hour + ":" + minute + ":" + "" + second).toString());
			responseModel.setContent(rContentModel);
			
			response = mapper.writeValueAsString(responseModel);
		} catch (Exception e) {
			// TODO: handle exception
			log.info(e.getStackTrace()+"");
		}
		return response;
	}
}
