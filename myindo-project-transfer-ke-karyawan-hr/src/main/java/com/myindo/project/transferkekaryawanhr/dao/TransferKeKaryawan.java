/**
 * 
 */
package com.myindo.project.transferkekaryawanhr.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Random;
import java.util.logging.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myindo.project.transferkekaryawanhr.connection.Connect;
import com.myindo.project.transferkekaryawanhr.model.HasilKar;
import com.myindo.project.transferkekaryawanhr.model.HasilPointHr;
import com.myindo.project.transferkekaryawanhr.model.RequestModel;

/**
 * @author Resti Pebriani
 *
 */
public class TransferKeKaryawan {
	Logger log = Logger.getLogger("TransferKeKaryawan");
	HasilPointHr hasilPointHr;
	HasilKar hasilKar ;
	String codeResponse;
	
	public String tfkeKaryawan(RequestModel rModel) {
		Connection con = Connect.connection();
		hasilPointHr = new HasilPointHr();
		hasilKar = new HasilKar();
		
		try {
			Random random =  new Random();
			int foto = random.nextInt(100000);
			String idString = Integer.toString(foto);
	        
			PreparedStatement pStm = con.prepareStatement("SELECT * FROM point_hr WHERE npk=?");
			pStm.setString(1, rModel.getNpk());
			ResultSet rSet = pStm.executeQuery();

			while (rSet.next()) {
				hasilPointHr.setIdPoinHr(rSet.getString("id_point_hr"));
				hasilPointHr.setNpk(rSet.getString("npk"));
				hasilPointHr.setTotalPoin(rSet.getString("total_point"));
			}
			rSet.close();
			
			ObjectMapper mapper = new ObjectMapper();
			String hsl = mapper.writeValueAsString(hasilPointHr);
			System.out.println("Hasil data poin HR : "+hsl);
			
			PreparedStatement pKar = con.prepareStatement("SELECT * FROM karyawan WHERE npk=?");
			pKar.setString(1, rModel.getNpk());
			ResultSet rKar = pKar.executeQuery();
			while (rKar.next()) {
				hasilKar.setNpk(rKar.getString("npk"));
				hasilKar.setNama(rKar.getString("nama"));
				hasilKar.setTotalPoin(rKar.getString("total_poin"));
			}
			rSet.close();
			
			int totPoin = Integer.parseInt(hasilPointHr.getTotalPoin());
			int rePoin = Integer.parseInt(rModel.getAmount());
			int poinKar = Integer.parseInt(hasilKar.getTotalPoin());
			
			int jmlPoinKar = rePoin + poinKar;
			int sisaPoin = totPoin - rePoin;
			
			String poinKarString = Integer.toString(jmlPoinKar);
			String poinString = Integer.toString(sisaPoin);
			
			System.out.println("Sisa Poin : "+sisaPoin);
			
			if (sisaPoin >= 0 && !hasilKar.getNama().trim().isEmpty() && !hasilPointHr.getIdPoinHr().trim().isEmpty()) {
				PreparedStatement ps = con.prepareStatement("INSERT INTO share_hr (id_share, id_point_hr, point_sent, date_trans, "
						+ "description, \"to\") VALUES (?, ?, ?::int, ?::date, ?, ?)");
				ps.setString(1, idString);
				ps.setString(2, hasilPointHr.getIdPoinHr());
				ps.setString(3, rModel.getAmount());
				ps.setString(4, rModel.getDateTransaction());
				ps.setString(5, rModel.getDescription());
				ps.setString(6, rModel.getTo());
				
				ps.executeUpdate();
				ps.close();
				
				codeResponse = "0000";
				log.info("Transfer poin ke karyawan berhasil");
			} else {
				codeResponse = "1111";
				log.info("Poin tidak cukup");
			}
			
			if (codeResponse.equals("0000")) {
				PreparedStatement pp = con.prepareStatement("UPDATE karyawan SET total_poin=? WHERE npk=?");
				pp.setString(1, poinKarString);
				pp.setString(2, rModel.getNpk());
				pp.executeUpdate();

				PreparedStatement poinHr = con
						.prepareStatement("UPDATE point_hr SET total_point=? WHERE npk=?");
				poinHr.setString(1, poinString);
				poinHr.setString(2, rModel.getNpk());
				poinHr.executeUpdate();

				log.info("Data poin berhasil di update");
			}
			con.close();
			
		} catch (Exception e) {
			// TODO: handle exception
			codeResponse = "1111";
			log.info("Proses Failed");
			log.info(e.getMessage());
		}
		return codeResponse;	
	}
}
